from machine import Timer

from core.classes.device import Device

device = Device()

led_timer_1 = Timer(-1)
led_timer_1.init(period=3000, callback=lambda t: device.switch_view())

led_timer_2 = Timer(-1)
led_timer_2.init(period=60000, callback=lambda t: device.update_weather_from_internet())

while True:
    pass
