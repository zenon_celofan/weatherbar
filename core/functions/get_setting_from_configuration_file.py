from core.exceptions import ConfigurationError


def get_setting_from_configuration_file(parameter_name):
    with open('/configuration.txt', 'r') as file:
        for line in file.readlines():
            if parameter_name in line:
                return line.strip().split('=')[1]
    raise ConfigurationError
