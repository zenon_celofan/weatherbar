import time

import ntptime
import urequests
from machine import Pin

from core.classes.node_mcu import NodeMcu
from core.classes.weather import Weather
from core.exceptions import WiFiError
from core.functions.get_setting_from_configuration_file import get_setting_from_configuration_file
from core.lib.max7219 import Max7219


class Device:
    def __init__(self):
        self.node_mcu = NodeMcu()
        self.weather = Weather()

        self.current_view = 0
        self.available_views = [
            lambda: self.display_rtc_time(),
            lambda: self.display_temperature()]

        self.screen = Max7219(
            width=32,
            height=8,
            spi=self.node_mcu.spi,
            cs=Pin(15))
        self.screen.brightness(0)

        self.screen.fill(0)
        self.screen.text('wifi', 1, 1, 1)
        self.screen.show()
        self.connect_to_wifi()

        self.screen.fill(0)
        self.screen.text('rtc', 1, 1, 1)
        self.screen.show()
        self.time_zone = int(get_setting_from_configuration_file('time_zone'))
        self.update_rtc_from_internet()

        self.screen.fill(0)
        self.screen.text('wthr', 1, 1, 1)
        self.screen.show()
        self.weather = Weather()
        self.update_weather_from_internet()

    def startup(self):
        self.node_mcu.debug_led.turn_on()
        self.connect_to_wifi()
        self.node_mcu.debug_led.turn_off()

    def connect_to_wifi(self):
        ssid = get_setting_from_configuration_file('wifi_name')
        password = get_setting_from_configuration_file('wifi_password')

        self.node_mcu.wlan.active(True)
        for attempt in range(10):
            self.node_mcu.wlan.connect(ssid, password)
            time.sleep(3)
            if self.node_mcu.wlan.isconnected():
                return
        raise WiFiError()

    def update_weather_from_internet(self):
        server_response = urequests.get(
            f'https://api.openweathermap.org/data/2.5/weather?q={self.weather.city},{self.weather.country}&APPID={self.weather.api_key}').json()
        self.weather.temperature = server_response['main']['temp'] - 273.15

    def display_temperature(self):
        self.screen.fill(0)
        self.screen.text(f'{self.weather.temperature:2.1f}', 1, 1, 1)
        self.screen.show()

    def update_rtc_from_internet(self):
        ct = time.gmtime(ntptime.time() + 3600 * self.time_zone)
        self.node_mcu.rtc.datetime((ct[0], ct[1], ct[2], ct[6] + 1, ct[3], ct[4], ct[5], 0))

    def display_rtc_time(self):
        hours = self.node_mcu.rtc.datetime()[4]
        minutes = self.node_mcu.rtc.datetime()[5]
        self.screen.fill(0)
        self.screen.text(f'{hours:2}{minutes:0>2}', 1, 1, 1)
        self.screen.show()

    def switch_view(self):
        self.current_view = (self.current_view + 1) % len(self.available_views)
        self.available_views[self.current_view]()
