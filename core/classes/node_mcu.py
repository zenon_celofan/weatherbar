import network
from machine import RTC, SPI

from core.classes.debug_led import DebugLed


class NodeMcu:
    def __init__(self):
        self.debug_led = DebugLed(2)
        self.debug_led.turn_off()

        self.wlan = network.WLAN(network.STA_IF)

        self.rtc = RTC()

        self.spi = SPI(1, baudrate=10000000)
