from core.functions.get_setting_from_configuration_file import get_setting_from_configuration_file


class Weather:
    def __init__(self):
        self.city = get_setting_from_configuration_file('weather_city')
        self.country = get_setting_from_configuration_file('weather_country')
        self.api_key = get_setting_from_configuration_file('openweathermap_api_key')

        self.temperature = 0
