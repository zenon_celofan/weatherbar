from machine import Pin


class DebugLed(Pin):
    def __init__(self, id):
        super().__init__(id, Pin.OUT)

    def turn_on(self):
        self.value(0)

    def turn_off(self):
        self.value(1)

    def toggle(self):
        self.value(not self.value())
