class WiFiError(Exception):
    """ WiFi connection error"""


class ConfigurationError(Exception):
    """ Configuration error"""
